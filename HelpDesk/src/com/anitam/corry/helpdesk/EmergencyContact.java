package com.anitam.corry.helpdesk;

public class EmergencyContact {
    	public String name;
    	public String phone;
    	
    	public EmergencyContact() {
    		super();
    	}
    	public EmergencyContact(String cName, String cPhone) {
    		super();
    		name = cName;
    		phone = cPhone;
    	}
    	public String getName() {
    		return name;
    	}
    	public String getPhone() {
    		return phone;
    	}
    	public String getString() {
    		return "Emergency Contact: "+name+"\nPhone: "+phone;
    	}
    }
