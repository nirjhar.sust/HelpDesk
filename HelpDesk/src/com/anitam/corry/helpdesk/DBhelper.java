package com.anitam.corry.helpdesk;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DBhelper extends SQLiteOpenHelper {
	
	private static final String CREATE_TABLE="create table " + 
			Constants.TABLE_NAME + " (" + 
			Constants.KEY_ID + " integer primary key autoincrement, " +
			Constants.CONTACT_NAME + " text not null, " + 
			Constants.CONTACT_PHONE + " text not null);";
	
	
	public DBhelper(Context context, String name, CursorFactory factory, int version) {
		super(context, name, factory, version);
	}
	
	
	@Override
	public void onCreate(SQLiteDatabase db) {
		Log.v("DBhelper onCreate", "Creating all the tables");
		try {
			db.execSQL(CREATE_TABLE);
		} catch(SQLiteException ex) {
			Log.v("Create table exception", ex.getMessage());
		}
	}
	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		Log.v("TaskDBAdapter", "Upgrading from version "+ oldVersion+ " to "+ newVersion+ ", which will destroy all old data");
		db.execSQL("drop table if exists " + Constants.TABLE_NAME);
		onCreate(db);
	}
	// This deletes the EMERGENCY CONTACTS table only, but DOES DELETE ALL ENTRIES
	public void deleteDB() {
		SQLiteDatabase db = this.getWritableDatabase();
        db.delete(Constants.TABLE_NAME, null, null);
        db.close();
	}
    public int getContactsCount() {
        String countQuery = "SELECT  * FROM " + Constants.TABLE_NAME;
        SQLiteDatabase db = this.getReadableDatabase();
        Cursor cursor = db.rawQuery(countQuery, null);
        
        return cursor.getCount();     
    }
} // End of DBhelper class

