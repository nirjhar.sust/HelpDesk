package com.anitam.corry.helpdesk;

import java.util.ArrayList;

import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.app.Activity;
import android.app.ListActivity;
import android.content.Context;
import android.content.CursorLoader;
import android.content.Intent;
import android.database.Cursor;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.AdapterView.AdapterContextMenuInfo;

public class Emergency_Contacts_2 extends ListActivity {

	private int PICK_CONTACT = 0;
	public EditText etContact, etContactPhone;
	private Button bAddContact, bDone;
	public EmergencyContact emergContacts= new EmergencyContact();
	EmergencyContactsAdapter ecAdapter;
	EContactsDB db;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		db = new EContactsDB(this);
		db.open();
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_emergency__contacts_2);

		etContact = (EditText)findViewById(R.id.etContactName);
		etContactPhone = (EditText)findViewById(R.id.etContactPhone);
		bAddContact = (Button)findViewById(R.id.bSaveContact);
		bDone = (Button)findViewById(R.id.bFinishActivity);

		ecAdapter = new EmergencyContactsAdapter(this);
		this.setListAdapter(ecAdapter);
		ecAdapter.initializeContactsList();
		ecAdapter.notifyDataSetChanged();
		this.registerForContextMenu(getListView());
		
		etContact.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
				if (etContact.length() < 1) {
					Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
				
			    startActivityForResult(contactPickerIntent, PICK_CONTACT);
				}				
			}
		});
        bAddContact.setOnClickListener(new View.OnClickListener() {
			@Override
			public void onClick(View v) {
	        	// Capture EditTexts again in case of changes to Contact's name 
				// or phone number by user AFTER they selected contact from 
				// phone's native list
				emergContacts.name = etContact.getText().toString();
	        	emergContacts.phone = etContactPhone.getText().toString();
	        	checkForOpenDB();
	        	
				if (!emergContacts.name.isEmpty() && !emergContacts.phone.isEmpty()) {
					if (!ecAdapter.checkPhoneForDuplicate(emergContacts.phone)) {
						ecAdapter.getData(emergContacts.name, emergContacts.phone);
						emergContacts.name = "";
						emergContacts.phone = "";
						ecAdapter.notifyDataSetChanged();
						etContact.setText("");
						etContactPhone.setText("");
					} else {
						etContactPhone.setText("");
						Toast.makeText(getBaseContext(), "This PHONE NUMBER already exists\nPlease enter a different number", Toast.LENGTH_SHORT).show();
					}

		        } else if ((emergContacts.name.isEmpty() & emergContacts.phone.isEmpty()) || emergContacts.name.isEmpty())
		        	Toast.makeText(getBaseContext(), "You have not selected a contact to save!", Toast.LENGTH_SHORT).show();
		        else
		        	Toast.makeText(getBaseContext(), "Please enter a phone number!", Toast.LENGTH_SHORT).show();
			}
		});
        bDone.setOnClickListener(new View.OnClickListener() {
        	@Override
        	public void onClick(View v) {

        		Toast.makeText(Emergency_Contacts_2.this, "Data Saved Succesfully.", Toast.LENGTH_SHORT).show();
        		finish();
        	}
        });
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_emergency__contacts_2, menu);
		return true;
	}

	/*								
	 *	START - EMERGENCY CONTACTS ADAPTER 
	 */
	private class EmergencyContactsAdapter extends BaseAdapter {
		private ArrayList<EmergencyContact> emergencyContacts;
		private LayoutInflater mInflater;

		public EmergencyContactsAdapter(Context context) {
			mInflater = LayoutInflater.from(context);
			emergencyContacts = new ArrayList<EmergencyContact>();
		}
		public boolean checkPhoneForDuplicate(String p) {
			if (db.getCount() > 0) {

				Cursor c = db.getAllContacts();

				c.moveToFirst();
				String phoneTemp;

				do {
					phoneTemp = c.getString(c.getColumnIndex(Constants.CONTACT_PHONE));
					if (p.equals(phoneTemp)) {
						c.close();
						return true;
					}
				}  while (c.moveToNext());
				c.close();
			}
			// Return false for validity...NO DUPLICATE PHONE NUMBER
			return false;
		}
		public void getData(String contact_name, String contact_phone) {

			EmergencyContact ecTemp = new EmergencyContact(contact_name, contact_phone);
			emergencyContacts.add(ecTemp);
			saveContactToDB(contact_name, contact_phone);
		}
		public void saveContactToDB(String contact_name, String contact_phone) {
			db.insertContact(contact_name, contact_phone);
		}
		public void initializeContactsList() {
			checkForOpenDB();
			Cursor cursor = db.getAllContacts();
			String cName, cPhone;

			if (cursor.getCount() > 0) {
				while (cursor.moveToNext()) {
					cName = cursor.getString(cursor.getColumnIndex(Constants.CONTACT_NAME));
					cPhone = cursor.getString(cursor.getColumnIndex(Constants.CONTACT_PHONE));
					EmergencyContact ecTemp = new EmergencyContact(cName, cPhone);
					emergencyContacts.add(ecTemp);
				}
			}

			cursor.close();
		}
		public void deleteContactFromDB(String cPhone) {
			checkForOpenDB();
			db.deleteContact(cPhone);
		}
		@Override
		public int getCount() {return emergencyContacts.size();}
		public EmergencyContact getItem(int i) {return emergencyContacts.get(i);}
		public long getItemId(int i) {return i;}
		public View getView(int arg0, View arg1, ViewGroup arg2) {
			final ViewHolder holder;
			View v = arg1;

			if ((v == null) || (v.getTag() == null)) {
				v = mInflater.inflate(R.layout.emergency_contacts_row, null);
				holder = new ViewHolder();
				holder.vhName = (TextView)v.findViewById(R.id.tvContactName);
				holder.vhPhone = (TextView)v.findViewById(R.id.tvContactPhone);
				v.setTag(holder);
			} else {
				holder = (ViewHolder)v.getTag();
			}
			holder.vhContact = getItem(arg0);
			holder.vhName.setText(holder.vhContact.name);
			holder.vhPhone.setText(holder.vhContact.phone);

			v.setTag(holder);
			return v;
		}
		public class ViewHolder {
			EmergencyContact vhContact;
			TextView vhName;
			TextView vhPhone;
		}
	}
	/*								
	 *	END - EMERGENCY CONTACTS ADAPTER 
	 */
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if ((requestCode == PICK_CONTACT) && (resultCode == RESULT_OK)) {

			String contactID;
			Uri contactData = data.getData();
			Cursor cursor;
			CursorLoader cl = new CursorLoader(this, contactData, null, null, null, null);
			cursor = cl.loadInBackground();
			
			if (cursor.moveToFirst()) {
				
				// Get user-picked contact's name and ID
				emergContacts.name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
				contactID = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));
				
				etContact.setText(emergContacts.name);
				int hasPhone = cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
				
				// Contact has a phone number so GET IT and SET IT
				if (hasPhone == 1) {
					
					Cursor phoneCursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, 
										ContactsContract.CommonDataKinds.Phone.CONTACT_ID+" = "+contactID, null, null);

					phoneCursor.moveToFirst();
					if (phoneCursor.getCount() < 1) {
						
						phoneCursor.close();
						Toast.makeText(this, "Person's PHONE NUMBER cannot be read\nPlease enter a number manually", Toast.LENGTH_SHORT).show();
						return;
					}
					
					emergContacts.phone = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
					phoneCursor.close();
					etContactPhone.setText(emergContacts.phone);
				}
			} else
				Toast.makeText(this, "Person's info is invalid or unavailable\nPlease try again", Toast.LENGTH_SHORT).show();
		}
	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		// TODO Auto-generated method stub
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.emergency_contacts, menu);
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		// TODO Auto-generated method stub
		AdapterContextMenuInfo info = (AdapterContextMenuInfo) item.getMenuInfo();
		switch (item.getItemId()) {
		case R.id.delete_contact:
			EmergencyContact contact = ecAdapter.emergencyContacts.get(info.position);
			if (db.getCount() == 1) {
				db.deleteContactTable();
			}
			ecAdapter.deleteContactFromDB(contact.phone);
			ecAdapter.emergencyContacts.remove(contact);
			ecAdapter.notifyDataSetChanged();
		default: 
			return super.onContextItemSelected(item);
		}
	}
	
	public void checkForOpenDB() {
		if (!db.open())
			db.open();
	}
	public void checkForClosedDB() {
		if (db.open())
			db.close();
	}
	@Override
	public void onPause() {
		super.onPause();
		checkForClosedDB();
	}

}
