package com.anitam.corry.helpdesk;

import android.app.Activity;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.CountDownTimer;
import android.util.Log;
import android.widget.Toast;

public class SimpleBroadcastReceiver extends BroadcastReceiver  {

	int count =0;

	KeyguardManager keyguardManager;
	KeyguardLock lock;
	boolean flag = true;

	@Override
	public void onReceive(final Context rcvContext, Intent rcvIntent) {

		keyguardManager = (KeyguardManager)rcvContext.getSystemService(Activity.KEYGUARD_SERVICE);
		lock = keyguardManager.newKeyguardLock(Activity.KEYGUARD_SERVICE);

		String action = rcvIntent.getAction();
		if (action.equals(Intent.ACTION_SCREEN_ON)) 
		{
			count++;
			if (flag)
			{
				CountDownTimer counter = new CountDownTimer(5000,1000) {

					@Override
					public void onTick(long millisUntilFinished) {
						flag = false;
						Log.v("count", String.valueOf(count));
						Log.v("flag", String.valueOf(flag));
						
					}
					
					@Override
					public void onFinish() {

						if (count>3)
						{
							Toast.makeText(rcvContext.getApplicationContext(), "testing", Toast.LENGTH_LONG).show();
							ShowDangerWindows(rcvContext);
						}
						flag = true;
						count = 0;
					}
				}.start();
				
				Toast.makeText(rcvContext.getApplicationContext(), "flag:"+String.valueOf(flag)+" count:"+String.valueOf(count), Toast.LENGTH_LONG).show();
				
			}
		}
		else if (action.equals(Intent.ACTION_SCREEN_OFF))
		{
			count++;
		}
	}

	public void ShowDangerWindows(Context rcvContext)
	{
		Intent startActivity = new Intent(); 
		startActivity.setClass(rcvContext, DangerMode.class); 
		startActivity.setAction(DangerMode.class.getName()); 
		startActivity.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS); 
		rcvContext.startActivity(startActivity);
	}
}