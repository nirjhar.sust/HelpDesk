package com.anitam.corry.helpdesk;

import java.util.ArrayList;
import java.util.List;

import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Emergency_Contacts extends Activity implements OnClickListener {

	Button choose1,choose2,choose3,choose4,save;
	TextView number1,number2,number3,number4;
	
	int count =0;
	protected static final int RESULT_SPEECH = 5;
	private static final int PICK_CONTACT = 0;
	public EmergencyContact emergContacts= new EmergencyContact();
	StringBuilder sb = new StringBuilder();
	
	List<String> listcontact= new ArrayList<String>();
	ArrayAdapter<String> listcontact_adapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_emergency__contacts);
		
		number1 = (TextView)findViewById(R.id.number1);
		number2 = (TextView)findViewById(R.id.number2);
		number3 = (TextView)findViewById(R.id.number3);
		number4 = (TextView)findViewById(R.id.number4);
		
		choose1 = (Button)findViewById(R.id.choose1_button);
		choose2 = (Button)findViewById(R.id.choose2_button);
		choose3 = (Button)findViewById(R.id.choose3_button);
		choose4 = (Button)findViewById(R.id.choose4_button);
		save = (Button)findViewById(R.id.Save_contacts_button);
		
		choose1.setOnClickListener(this);
		choose2.setOnClickListener(this);
		choose3.setOnClickListener(this);
		choose4.setOnClickListener(this);
		save.setOnClickListener(this);
		
		PullData();

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_emergency__contacts, menu);
		return true;
	}

	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub

		if (v.getId()==R.id.choose1_button)
		{
			count  =1;
			Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
			startActivityForResult(contactPickerIntent, PICK_CONTACT);
		}
		else if (v.getId()==R.id.choose2_button)
		{
			count =2;
			Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
			startActivityForResult(contactPickerIntent, PICK_CONTACT);
		}
		else if (v.getId()==R.id.choose3_button)
		{
			count =3;
			Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
			startActivityForResult(contactPickerIntent, PICK_CONTACT);

		}
		else if (v.getId()==R.id.choose4_button)
		{
			count =4;
			Intent contactPickerIntent = new Intent(Intent.ACTION_PICK, ContactsContract.Contacts.CONTENT_URI);
			startActivityForResult(contactPickerIntent, PICK_CONTACT);
		}
		else if (v.getId()==R.id.Save_contacts_button)
		{
			String num1 = number1.getText().toString();
			String num2 = number2.getText().toString();
			String num3 = number3.getText().toString();
			String num4 = number4.getText().toString();
			if (num1!="" && num2!="" && num3!="" && num4!="")
			{
				String all_number_string = number1.getText().toString()+"   "+number2.getText().toString()+"   "+number3.getText().toString()+"   "+number4.getText().toString();
				String all_name_string = choose1.getText().toString()+"   "+choose2.getText().toString()+"   "+choose3.getText().toString()+"   "+choose4.getText().toString();
				AlertDialog art = new AlertDialog.Builder(this).create();
				if (SaveContacts(all_number_string) && SaveContactName(all_name_string))
				{
					art.setMessage("Data has been saved successfully.");
					art.show();
				}
				else
				{
					art.setMessage("There was a problem saving data.");
					art.show();
				}
			}
			else
			{
				AlertDialog art = new AlertDialog.Builder(this).create();
				art.setMessage("You must have to choose all 4 numbers.");
				art.show();
			}
		}

	}
	
	public boolean SaveContacts(String TheMainString)
	{
		try
		{
			SharedPreferences sharedPreferences = getSharedPreferences("HelpDesk",MODE_PRIVATE);
		    SharedPreferences.Editor editor = sharedPreferences.edit();
		    editor.putString("Emergency_Contacts",TheMainString);
		    editor.commit();
			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}
	
	public boolean SaveContactName(String TheMainString)
	{
		try
		{
			SharedPreferences sharedPreferences = getSharedPreferences("HelpDesk",MODE_PRIVATE);
		    SharedPreferences.Editor editor = sharedPreferences.edit();
		    editor.putString("Emergency_Contact_Names",TheMainString);
		    editor.commit();
			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}
	
	public void PullData()
	{
		try
		{
			SharedPreferences sharedPreferences = getSharedPreferences("HelpDesk",MODE_PRIVATE);
		    String strSavedMem1 = sharedPreferences.getString("Emergency_Contacts", "");
		    String nameString = sharedPreferences.getString("Emergency_Contact_Names", "");
		    String[] arr = {};
		    arr = strSavedMem1.split("   ");
		    number1.setText(arr[0]);
	    	number2.setText(arr[1]);
	    	number3.setText(arr[2]);
	    	number4.setText(arr[3]);
	    	
	    	String[] names = {};
	    	names = nameString.split("   ");
	    	choose1.setText(names[0]);
	    	choose2.setText(names[1]);
	    	choose3.setText(names[2]);
	    	choose4.setText(names[3]);
	    	
		}
		catch (Exception e)
		{
			AlertDialog art = new AlertDialog.Builder(this).create();
			art.setMessage("There was some problems to pull data.");
			art.show();
		}
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		String number ="";
		if ((requestCode == PICK_CONTACT) && (resultCode == RESULT_OK)) {

			String contactID;
			Uri contactData = data.getData();
			Cursor cursor;
			CursorLoader cl = new CursorLoader(this, contactData, null, null, null, null);
			cursor = cl.loadInBackground();

			if (cursor.moveToFirst()) {

				// Get user-picked contact's name and ID
				emergContacts.name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
				contactID = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID));

				//etContact.setText(emergContacts.name);
				String name = emergContacts.name;
				int hasPhone = cursor.getInt(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER));
				
				
				// Contact has a phone number so GET IT and SET IT
				if (hasPhone == 1) {
					Cursor phoneCursor = getContentResolver().query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, 
							ContactsContract.CommonDataKinds.Phone.CONTACT_ID+" = "+contactID, null, null);
					phoneCursor.moveToFirst();
					emergContacts.phone = phoneCursor.getString(phoneCursor.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
					phoneCursor.close();
					//etContactPhone.setText(emergContacts.phone);
					number = emergContacts.phone;
				}
				else if (hasPhone==0)
				{
					number = "<Not Available>";
				}
				if (count==1 && number!="<Not Available>")
				{
					choose1.setText(name);
					number1.setText(number);
				}
				else if (count==2 && number!="<Not Available>")
				{
					choose2.setText(name);
					number2.setText(number);
				}
				else if (count==3 && number!="<Not Available>")
				{
					choose3.setText(name);
					number3.setText(number);

				}
				else if (count==4 && number!="<Not Available>")
				{
					choose4.setText(name);
					number4.setText(number);
				}
				else
				{
					AlertDialog art = new AlertDialog.Builder(this).create();
					art.setMessage("Invalid Contact. Please Select another one.");
					art.show();
				}
			} else
				Toast.makeText(this, "Person's info is invalid or unavailable\nPlease try again", Toast.LENGTH_SHORT).show();
		}
	}

}
