package com.anitam.corry.helpdesk;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.Timer;
import java.util.TimerTask;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONException;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.app.AlertDialog;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.util.Log;
import android.widget.Toast;

public class MyWarningService extends Service implements LocationListener  {

	int count;
	SimpleBroadcastReceiver intentReceiver = new SimpleBroadcastReceiver();
	IntentFilter intentFilter = new IntentFilter(Intent.ACTION_SCREEN_ON);
	Thread background;
	Timer timer;
	TimerTask hourlyTask;
	LocationManager mLocationManager;
	Location mLocation;
	
	Handler handler = new Handler() {
		@SuppressWarnings("unchecked")
		@Override
		public void handleMessage(Message msg) {
			//display each item in a single line
			String str = String.valueOf(msg.obj);
			String[] coordnt = {};
			coordnt = str.split(",");
			double lat = Double.parseDouble(coordnt[0]);
			double lon = Double.parseDouble(coordnt[1]);
			
			Criteria criteria = new Criteria();
			String bestProvider = mLocationManager.getBestProvider(criteria, false);
			mLocation = mLocationManager.getLastKnownLocation(bestProvider);
			mLocationManager.requestLocationUpdates(bestProvider, 5000, (float) 2.0, MyWarningService.this);
			
			double lat2 = mLocation.getLatitude ();
			double lon2 = mLocation.getLongitude ();
			
			double miles = distance(lon, lat, lat2, lon2);
			
			if (miles <=5)
			{
				Intent startActivity = new Intent(); 
				startActivity.setClass(MyWarningService.this, DangerMode.class); 
				startActivity.setAction(DangerMode.class.getName()); 
				startActivity.setFlags( Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_EXCLUDE_FROM_RECENTS); 
				startActivity(startActivity);
			}
			
		}
	};
	
	@Override
	public void onCreate() {
		
		intentFilter.addAction(Intent.ACTION_SCREEN_OFF);
		registerReceiver(intentReceiver, intentFilter);
		
		try
		{
			mLocationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
			Criteria criteria = new Criteria();
			criteria.setAccuracy(Criteria.ACCURACY_FINE);
			criteria.setPowerRequirement(Criteria.POWER_LOW);
			String locationprovider =mLocationManager.getBestProvider(criteria,true);
			mLocation = mLocationManager.getLastKnownLocation(locationprovider);
			mLocationManager.requestLocationUpdates(locationprovider, 60*1000, (float) 2.0, this);
		}
		catch (NullPointerException e)
		{
			AlertDialog  art = new AlertDialog.Builder(MyWarningService.this).create();
			art.setMessage("Cant Get your Last Location.");
			art.show();
		}
		
		Toast.makeText(this, "Heldesk Service Started.", Toast.LENGTH_LONG).show();
	}
	
	@Override
	public IBinder onBind(Intent intent) {
		return null;
	}

	public String[] GetAddressList()
	{
		try
		{
			SharedPreferences sharedPreferences = getSharedPreferences("HelpDesk",MODE_PRIVATE);
		    String strSavedMem1 = sharedPreferences.getString("Dangerous_Addresses", "");
		    String[] arr = {};
		    arr = strSavedMem1.split("###");
		    return arr;
		   		    
		}
		catch (Exception e)
		{
			String[] blank = {};
			return blank;
		}
	}

	private double distance (double lat1, double lon1, double lat2, double lon2) {
	  double theta = lon1 - lon2;
	  double dist = Math.sin(deg2rad(lat1)) * Math.sin(deg2rad(lat2)) + Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) * Math.cos(deg2rad(theta));
	  dist = Math.acos(dist);
	  dist = rad2deg(dist);
	  dist = dist * 60 * 1.1515;
	  return (dist);
	}

	private double deg2rad(double deg) {
	  return (deg * Math.PI / 180.0);
	}

	private double rad2deg(double rad) {
	  return (rad * 180 / Math.PI);
	}

	
	@Override
	public void onStart(Intent intent, int startId) {
		super.onStart(intent, startId);

		
		
		timer = new Timer ();
		hourlyTask = new TimerTask () {
		    @Override
		    public void run () {
		    	
		    	String[] alladdresses = {};
				alladdresses = GetAddressList();
		    	
		    	for (String temp: alladdresses)
		    	{
		    		try {
		    			String aaa = temp.replace(" ", "%20");
					    String a = ProcessResponse(SearchRequest(aaa));
						Message msg = new Message();
						msg.obj = (Object)a;
						handler.sendMessage(msg);
					} catch (IllegalStateException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (NoSuchAlgorithmException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (MalformedURLException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
		    	}
		    	
//		    	String address ="WTF android";
//				Message a = new Message();
//				a.obj = (Object)address;
//				handler.sendMessage(a);
		    }
		};
		timer.schedule(hourlyTask, 0l, 15*1000);
	
		
		Toast.makeText(this, "Timer Starts.", Toast.LENGTH_LONG).show();
	}
	
	public String SearchRequest(String searchString) throws MalformedURLException, IOException {
		String newFeed="http://maps.google.co.uk/maps/geo?q="+searchString+"&output=xml";
		StringBuilder response = new StringBuilder();
		Log.v("gsearch","gsearch url:"+newFeed);
		URL url = new URL(newFeed);
		HttpURLConnection httpconn = (HttpURLConnection) url.openConnection();
		if(httpconn.getResponseCode()==HttpURLConnection.HTTP_OK) {
			BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()),8192);
			String strLine = null;
			while ((strLine = input.readLine()) != null)
			{
				response.append(strLine);
			}
			input.close();
		}
		return response.toString();
		//tv.setText(response.toString());
	}

	public String ProcessResponse(String resp) throws IllegalStateException,
	IOException, JSONException, NoSuchAlgorithmException {

		Log.v("gsearch","gsearch result:"+resp);
		String add = "";
		try
		{
			Document doc = XMLfromString(resp);
			NodeList list = doc.getElementsByTagName("coordinates");
			add = list.item(0).getTextContent().toString();
			
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return add;
	}

	public Document XMLfromString(String v){

		Document doc = null;

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {

			DocumentBuilder db = dbf.newDocumentBuilder();

			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(v));
			doc = db.parse(is); 

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			System.out.println("Wrong XML file structure: " + e.getMessage());
			return null;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return doc;

	}

	
	
	@Override

	public void onDestroy() {
		super.onDestroy();
		
		hourlyTask.cancel();
		timer.cancel();
//		background.stop();
		unregisterReceiver(intentReceiver);
		Toast.makeText(this, "Heldesk Service Cancelled !", Toast.LENGTH_LONG).show();

	}
	
	@Override
	public boolean onUnbind(Intent intent) {
		return super.onUnbind(intent);

	}

	@Override
	public void onLocationChanged(Location arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderEnabled(String arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onStatusChanged(String arg0, int arg1, Bundle arg2) {
		// TODO Auto-generated method stub
		
	}

}
