package com.anitam.corry.helpdesk;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class ShowHelp extends Activity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_show_help);
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_show_help, menu);
		return true;
	}

}
