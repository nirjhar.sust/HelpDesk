package com.anitam.corry.helpdesk;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.SharedPreferences;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.MenuItem.OnMenuItemClickListener;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

public class EmergencyAddress extends Activity implements OnClickListener {

	ListView addresslistfromgoogle,currentaddresslist;
	Button save_address,lookupaddress;
	EditText addresstext;
	
	List<String> thelist = new ArrayList<String>();
	ArrayAdapter<String> ListAdapter;
	
	List<String> currentlist = new ArrayList<String>();
	ArrayAdapter<String> currentlistadapter;
	
	String[] items = new String[20];
	Handler handler = new Handler() {
		@SuppressWarnings("unchecked")
		@Override
		public void handleMessage(Message msg) {
			//display each item in a single line
			String a =  null;
			a= String.valueOf(msg.obj);
			addresstext.setText(a);
			items = a.split("###");
			for (int i=0; i<items.length; i++)
			{
				if (items[i].length()>5)
				{
					thelist.add(items[i].toString());
				}
			}
			ListAdapter = new ArrayAdapter<String>(EmergencyAddress.this,android.R.layout.simple_list_item_1, thelist );
			addresslistfromgoogle.setAdapter(ListAdapter);
			addresstext.setText("");
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_emergency_address);

		addresslistfromgoogle = (ListView)findViewById(R.id.addresslist_from_google);
		currentaddresslist = (ListView)findViewById(R.id.current_addresslist);
		save_address = (Button)findViewById(R.id.save_addresses);
		lookupaddress = (Button)findViewById(R.id.look_up_address);
		addresstext = (EditText)findViewById(R.id.address_text);
		save_address.setOnClickListener(this);
		lookupaddress.setOnClickListener(this);
		currentlistadapter = new ArrayAdapter<String>(EmergencyAddress.this, android.R.layout.simple_list_item_1,currentlist );
		currentaddresslist.setAdapter(currentlistadapter);
		addresslistfromgoogle.setOnItemLongClickListener(new android.widget.AdapterView.OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1, int arg2,
					long arg3) {
				Object itemobj = addresslistfromgoogle.getItemAtPosition(arg2);
				String item = String.valueOf(itemobj);
				boolean flag = false;
				for (int i=0; i<currentlist.size(); i++)
				{
					if (currentlist.get(i).toString()==item)
					{
						flag = true;
						break;
					}
				}
				if (flag==false)
				{
					currentlist.add(item);
					currentlistadapter.notifyDataSetChanged();
				}
				else
				{
					AlertDialog art = new AlertDialog.Builder(EmergencyAddress.this).create();
					art.setMessage("This Place is already enlisted.");
					art.show();
				}
				return flag;
			}
		});
		currentaddresslist.setOnItemLongClickListener(new OnItemLongClickListener() {

			@Override
			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,int arg2, long arg3) {
				
				Object itemobj = addresslistfromgoogle.getItemAtPosition(arg2);
				currentlist.remove(itemobj);
				currentlistadapter.notifyDataSetChanged();
				return false;
			}
		});
		
		GetData();
	}

	public String SearchRequest(String searchString) throws MalformedURLException, IOException {
		String newFeed="http://maps.google.co.uk/maps/geo?q="+searchString+"&output=xml";
		StringBuilder response = new StringBuilder();
		Log.v("gsearch","gsearch url:"+newFeed);
		URL url = new URL(newFeed);
		HttpURLConnection httpconn = (HttpURLConnection) url.openConnection();
		if(httpconn.getResponseCode()==HttpURLConnection.HTTP_OK) {
			BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()),8192);
			String strLine = null;
			while ((strLine = input.readLine()) != null)
			{
				response.append(strLine);
			}
			input.close();
		}
		return response.toString();
		//tv.setText(response.toString());
	}

	public String ProcessResponse(String resp) throws IllegalStateException,
	IOException, JSONException, NoSuchAlgorithmException {

		Log.v("gsearch","gsearch result:"+resp);
		String add = "";
		try
		{
			Document doc = XMLfromString(resp);
			NodeList list = doc.getElementsByTagName("address");
			for (int i =0; i<list.getLength(); i++)
			{
				Node a = list.item(i);
				add = add + a.getTextContent().toString()+ "###"; 
			}
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return add;
	}

	public Document XMLfromString(String v){

		Document doc = null;

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {

			DocumentBuilder db = dbf.newDocumentBuilder();

			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(v));
			doc = db.parse(is); 

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			System.out.println("Wrong XML file structure: " + e.getMessage());
			return null;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return doc;

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_emergency_address, menu);
		
		MenuItem item = menu.add("dhaka");
		item.setOnMenuItemClickListener(new OnMenuItemClickListener() {
			
			public boolean onMenuItemClick(MenuItem item) {

				addresstext.setText("pocket");
				return false;
			}
		});
		return true;
	}

	@Override
	public void onClick(View v) {

		if (v.getId()==R.id.look_up_address)
		{
			Thread background=new Thread(new Runnable() {

				@Override
				public void run() {

					try
					{
						String address = null;
						address = ProcessResponse(SearchRequest(addresstext.getText().toString().toLowerCase().replace(" ", "%20")));

						Message a = new Message();
						a.obj = (Object)address;
						handler.sendMessage(a);
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}
				}
			});
			background.start();
			
			
		}
		else if (v.getId()==R.id.save_addresses)
		{
			String dang_address="";
			for (String a: currentlist)
			{
				dang_address= dang_address+a+"###";
			}
			
			boolean flag = SaveData(dang_address);
			if (flag)
			{
				Toast.makeText(this, "Data Saved Succesfully.", Toast.LENGTH_SHORT).show();
				finish();
			}
			else
			{
				Toast.makeText(this, "Data was not Saved Succesfully.", Toast.LENGTH_SHORT).show();
			}
		}
	}
	
	public boolean SaveData(String TheMainString)
	{
		try
		{
			SharedPreferences sharedPreferences = getSharedPreferences("HelpDesk",MODE_PRIVATE);
		    SharedPreferences.Editor editor = sharedPreferences.edit();
		    editor.putString("Dangerous_Addresses",TheMainString);
		    editor.commit();
			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}
	
	public void GetData()
	{
		try
		{
			SharedPreferences sharedPreferences = getSharedPreferences("HelpDesk",MODE_PRIVATE);
		    String strSavedMem1 = sharedPreferences.getString("Dangerous_Addresses", "");
		    String[] arr = {};
		    arr = strSavedMem1.split("###");
		    for (String a: arr)
		    {
		    	currentlist.add(a);
		    }
		    currentlistadapter.notifyDataSetChanged();
		    
		}
		catch (Exception e)
		{
			AlertDialog art = new AlertDialog.Builder(this).create();
			art.setMessage("There was some problems to pull data.");
			art.show();
		}
	}

}
