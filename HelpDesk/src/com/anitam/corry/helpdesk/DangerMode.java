package com.anitam.corry.helpdesk;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.NoSuchAlgorithmException;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.json.JSONException;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Vibrator;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.KeyguardManager;
import android.app.KeyguardManager.KeyguardLock;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class DangerMode extends Activity implements OnClickListener,LocationListener{

	Button i_am_ok, i_am_not_ok;
	LocationManager mLocationManager;
	Location mLocation;
	String LastAddress="";
	KeyguardManager keyguardManager;
	KeyguardLock lock;
	Vibrator myVib;
	
	Handler handler = new Handler() {
		@SuppressWarnings("unchecked")
		@Override
		public void handleMessage(Message msg) {
			//display each item in a single line
			String a =  null;
			a= String.valueOf(msg.obj);
			
			SharedPreferences sharedPreferences = getSharedPreferences("HelpDesk",MODE_PRIVATE);
		    String strSavedMem1 = sharedPreferences.getString("Username", "");
		    String agestr = sharedPreferences.getString("Default_Message", "");
			
			SaveData(a);
			
			String message = strSavedMem1 + "_" +agestr+"_"+a;
			LastAddress = a;
			AlertDialog art = new AlertDialog.Builder(DangerMode.this).create();
			art.setMessage(message);
			art.show();
			SendMessage(message);
		}
	};

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_danger_mode);

		keyguardManager = (KeyguardManager)getSystemService(Activity.KEYGUARD_SERVICE);
		lock = keyguardManager.newKeyguardLock(Activity.KEYGUARD_SERVICE);
		
		i_am_ok = (Button)findViewById(R.id.iamok_button);
		i_am_not_ok = (Button)findViewById(R.id.iam_not_ok_button);
		i_am_ok.setOnClickListener(this);
		i_am_not_ok.setOnClickListener(this);

		try
		{

			mLocationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
			Criteria criteria = new Criteria();
			criteria.setAccuracy(Criteria.ACCURACY_FINE);
			criteria.setPowerRequirement(Criteria.POWER_LOW);
			String locationprovider =mLocationManager.getBestProvider(criteria,true);
			mLocation = mLocationManager.getLastKnownLocation(locationprovider);
			mLocationManager.requestLocationUpdates(locationprovider, 5000, (float) 2.0, this);
		}
		catch (NullPointerException e)
		{
			AlertDialog  art = new AlertDialog.Builder(DangerMode.this).create();
			art.setMessage("Cant Get your Last Location.");
			art.show();
		}
	}
	
	public boolean SaveData(String TheMainString)
	{
		try
		{
			SharedPreferences sharedPreferences = getSharedPreferences("HelpDesk",MODE_PRIVATE);
		    SharedPreferences.Editor editor = sharedPreferences.edit();
		    editor.putString("Last_address",TheMainString);
		    editor.commit();
			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}
	
	@Override
	protected void onResume() {
		// TODO Auto-generated method stub
		myVib = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
		
		long[] pattern = {2000,1000,5000};
		myVib.vibrate(pattern,1);
		
		lock.disableKeyguard();
		super.onResume();
	}

	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
//		AlertDialog  art = new AlertDialog.Builder(DangerMode.this).create();
//		art.setMessage("OnPause");
//		art.show();
		myVib.cancel();
		lock.reenableKeyguard();
		super.onPause();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_danger_mode, menu);
		return true;
	}


	public String SearchRequest(String searchString) throws MalformedURLException, IOException {
		String newFeed="http://maps.google.co.uk/maps/geo?q="+searchString+"&output=xml";
		StringBuilder response = new StringBuilder();
		Log.v("gsearch","gsearch url:"+newFeed);
		URL url = new URL(newFeed);
		HttpURLConnection httpconn = (HttpURLConnection) url.openConnection();
		if(httpconn.getResponseCode()==HttpURLConnection.HTTP_OK) {
			BufferedReader input = new BufferedReader(new InputStreamReader(httpconn.getInputStream()),8192);
			String strLine = null;
			while ((strLine = input.readLine()) != null)
			{
				response.append(strLine);
			}
			input.close();
		}
		return response.toString();
		//tv.setText(response.toString());
	}

	public String ProcessResponse(String resp) throws IllegalStateException,
	IOException, JSONException, NoSuchAlgorithmException {

		new StringBuilder();
		Log.v("gsearch","gsearch result:"+resp);
		String add = "";
		try
		{
			Document doc = XMLfromString(resp);
			NodeList list = doc.getElementsByTagName("address");
			Node a = list.item(0);
			add = a.getTextContent().toString();
		}
		catch (Exception e)
		{
			e.printStackTrace();
		}
		return add;
	}

	public Document XMLfromString(String v){

		Document doc = null;

		DocumentBuilderFactory dbf = DocumentBuilderFactory.newInstance();
		try {

			DocumentBuilder db = dbf.newDocumentBuilder();

			InputSource is = new InputSource();
			is.setCharacterStream(new StringReader(v));
			doc = db.parse(is); 

		} catch (ParserConfigurationException e) {
			e.printStackTrace();
		} catch (SAXException e) {
			System.out.println("Wrong XML file structure: " + e.getMessage());
			return null;
		} catch (IOException e) {
			e.printStackTrace();
		}

		return doc;

	}

	EContactsDB contactsDB;
	SmsManager emergencyTextMsg = SmsManager.getDefault();
	
	public void SendMessage(String Msg)
	{
		contactsDB = new EContactsDB(getBaseContext());
		contactsDB.open();
		if (contactsDB.getCount() > 0) {
			Cursor cursor = contactsDB.getAllContacts();
			cursor.moveToFirst();

			do {
				String smsNumber = cursor.getString(cursor.getColumnIndex(Constants.CONTACT_PHONE));
				emergencyTextMsg.sendTextMessage(smsNumber, null, Msg, null, null);
				
			} while (cursor.moveToNext());
		}
		AlertDialog art = new  AlertDialog.Builder(this).create();
		art.setMessage("Text Message Sent to all Emergency contacts.");
		art.show();
	}
	
	@Override
	public void onClick(View v) {
		if (v.getId()==R.id.iamok_button)
		{
			IamOK();
		}
		else if (v.getId()==R.id.iam_not_ok_button)
		{
			IamNotOK();
		}

	}
	@Override
	public boolean onKeyDown(int keyCode, KeyEvent event) {
		// TODO Auto-generated method stub
		
		
		if (keyCode==KeyEvent.KEYCODE_VOLUME_UP)
		{
			IamOK();
		}
		else if (keyCode==KeyEvent.KEYCODE_VOLUME_DOWN)
		{
			IamNotOK();
		}
		return super.onKeyDown(keyCode, event);
		
		
	}
	
	public void IamOK()
	{
		lock.reenableKeyguard();
		finish();
	}
	
	public void IamNotOK()
	{
		LocationManager locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		Criteria criteria = new Criteria();
		String bestProvider = locationManager.getBestProvider(criteria, false);
		mLocation = locationManager.getLastKnownLocation(bestProvider);
		mLocationManager.requestLocationUpdates(bestProvider, 5000, (float) 2.0, this);
		
		try {
			double lat = mLocation.getLatitude ();
			double lon = mLocation.getLongitude ();
			final String query_string = String.valueOf(lat)+"%20"+String.valueOf(lon);
			
			Thread background=new Thread(new Runnable() {

				@Override
				public void run() {

					try
					{
						String address = null;
						address = ProcessResponse(SearchRequest(query_string));

						Message a = new Message();
						a.obj = (Object)address;
						handler.sendMessage(a);
					}
					catch (Exception e)
					{
						e.printStackTrace();
					}
				}
			});
			background.start();
		}
		catch (NullPointerException e){
			AlertDialog  art = new AlertDialog.Builder(DangerMode.this).create();
			art.setMessage("Can't Get your last location.");
			art.show();
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderDisabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub

	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}

}
