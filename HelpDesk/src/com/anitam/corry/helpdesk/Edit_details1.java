package com.anitam.corry.helpdesk;

import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class Edit_details1 extends Activity implements OnClickListener {
	
	EditText  username,age;
	Button add_contacts, add_address,save;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_edit_details1);
		
		username = (EditText)findViewById(R.id.username);
		age = (EditText)findViewById(R.id.age);
		add_contacts = (Button)findViewById(R.id.add_contacts);
		add_address = (Button)findViewById(R.id.add_address);
		save = (Button)findViewById(R.id.Save);
		add_address.setOnClickListener(this);
		add_contacts.setOnClickListener(this);
		save.setOnClickListener(this);
		
		try
		{
			SharedPreferences sharedPreferences = getSharedPreferences("HelpDesk",MODE_PRIVATE);
		    String strSavedMem1 = sharedPreferences.getString("Username", "");
		    String agestr = sharedPreferences.getString("Default_Message", "");
			username.setText(strSavedMem1);
			age.setText(agestr);
		}
		catch (Exception e)
		{
			username.setText("");
			age.setText("");
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_edit_details1, menu);
		return true;
	}

	private boolean isNetworkAvailable() 
	{
	    ConnectivityManager connectivityManager 
	          = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
	    NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
	    return activeNetworkInfo != null;
	}
	
	public boolean SaveData(String Username,String Age)
	{
		try
		{
			SharedPreferences sharedPreferences = getSharedPreferences("HelpDesk",MODE_PRIVATE);
		    SharedPreferences.Editor editor = sharedPreferences.edit();
		    editor.putString("Default_Message",Age);
		    editor.putString("Username",Username);
		    editor.commit();
			return true;
		}
		catch (Exception e)
		{
			return false;
		}
	}
	
	public boolean GetData()
	{
		try
		{
			SharedPreferences sharedPreferences = getSharedPreferences("HelpDesk",MODE_PRIVATE);
		    String strSavedMem1 = sharedPreferences.getString("Dangerous_Addresses", "");
		    String[] arr = {};
		    arr = strSavedMem1.split("###");
		    
		    String numbers = sharedPreferences.getString("Emergency_Contacts", "");
		    String[] brr = {};
		    brr = numbers.split("damn");
		    if (arr.length >0 && brr.length >0)
		    {
		    	return true;
		    }
		    else
		    {
		    	return false;
		    }
		}
		catch (Exception e)
		{
			return false;
		}
	}
	
	@Override
	public void onClick(View v) {
		
		
		if (v.getId()==R.id.add_address)
		{
			startActivity(new Intent(this,EmergencyAddress.class));
		}
		else if ( v.getId()==R.id.add_contacts)
		{
			startActivity(new Intent(this,Emergency_Contacts_2.class));
			
		}
		else if (v.getId()==R.id.Save)
		{
			AlertDialog art = new AlertDialog.Builder(this).create();
			
			if (username.getText().toString()!="")
			{
				if (SaveData(username.getText().toString(),age.getText().toString()))
				{
					if (GetData())
					{
						if (isNetworkAvailable())
						{
							startService(new Intent(this,MyWarningService.class));
							finish();
						}
						else 
						{
							art.setMessage("Internet not available. Service CANT Start.");
							art.show();
						}
					}
					else
					{
						art.setMessage("Couldn't retrieve data successfully. Service CANT Start.");
						art.show();
					}
				}
				else
				{
					art.setMessage("UserName and Default Message was not saved successfully. Service CANT Start.");
					art.show();
				}
			}
			else
			{
				art.setMessage("Please Enter USER NAME. Service CANT Start.");
				art.show();
			}
		}
		

		
	}

}
