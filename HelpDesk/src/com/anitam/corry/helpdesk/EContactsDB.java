package com.anitam.corry.helpdesk;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.util.Log;

public class EContactsDB {
	private SQLiteDatabase db;
	private Context context;
	private final DBhelper dbhelper;
	
	public EContactsDB(Context c) {
		context = c;
		dbhelper = new DBhelper(context, Constants.DATABASE_NAME, null, Constants.DATABASE_VERSION);
	}
	public void close() {
		db.close();
	}
	public boolean open() throws SQLiteException {
		try {
			db = dbhelper.getWritableDatabase();
			
		} catch(SQLiteException ex) {
			Log.v("Open database exception caught", ex.getMessage());
			db = dbhelper.getReadableDatabase();
			return false;
		}
		return true;
	}
	public long insertContact(String contact_name, String contact_phone) {
		try {
			ContentValues newContactValue = new ContentValues();
			newContactValue.put(Constants.CONTACT_NAME, contact_name);
			newContactValue.put(Constants.CONTACT_PHONE, contact_phone);
			return db.insert(Constants.TABLE_NAME, null, newContactValue);
		}	catch(SQLiteException ex) {
			Log.v("Insert into database exception caught", ex.getMessage());
			return -1;
		}
	}
	public void deleteContact(String cPhone) {
		db.delete(Constants.TABLE_NAME, Constants.CONTACT_PHONE+"="+cPhone, null);
	}
	public void deleteContactTable() {
		db.delete(Constants.TABLE_NAME, null, null);
		db.close();
	}
	public Cursor getAllContacts() {
		Cursor c = db.query(Constants.TABLE_NAME, null, null, null, null, null, null);
		return c;
	}
	public int getCount() {
		return dbhelper.getContactsCount();
	}
	
} // End of EContactsDB class


