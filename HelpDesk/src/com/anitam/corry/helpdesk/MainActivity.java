package com.anitam.corry.helpdesk;

import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Vibrator;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.view.Menu;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;

public class MainActivity extends Activity implements OnClickListener,LocationListener {

	private Button edit_text,show_me,exit,deactivate;
	LocationManager mLocationManager;
	Location mLocation;
	
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);

		edit_text = (Button)findViewById(R.id.edit_user_button);
		show_me = (Button)findViewById(R.id.show_me);
		exit = (Button)findViewById(R.id.exit_button);
		deactivate = (Button)findViewById(R.id.deactivate_helpdesk);
		
		edit_text.setOnClickListener(this);
		show_me.setOnClickListener(this);
		exit.setOnClickListener(this);
		deactivate.setOnClickListener(this);
		
		try
		{

			mLocationManager = (LocationManager)getSystemService(Context.LOCATION_SERVICE);
			Criteria criteria = new Criteria();
			criteria.setAccuracy(Criteria.ACCURACY_FINE);
			criteria.setPowerRequirement(Criteria.POWER_LOW);
			String locationprovider =mLocationManager.getBestProvider(criteria,true);
			mLocation = mLocationManager.getLastKnownLocation(locationprovider);
			mLocationManager.requestLocationUpdates(locationprovider, 5000, (float) 2.0, this);
		}
		catch (NullPointerException e)
		{
			AlertDialog  art = new AlertDialog.Builder(MainActivity.this).create();
			art.setMessage("Cant Get your Last Location.");
			art.show();
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.activity_main, menu);
		menu.add("Wass upp");
		return true;
	}

	public String[] GetAddressList()
	{
		try
		{
			SharedPreferences sharedPreferences = getSharedPreferences("HelpDesk",MODE_PRIVATE);
		    String strSavedMem1 = sharedPreferences.getString("Dangerous_Addresses", "");
		    String[] arr = {};
		    arr = strSavedMem1.split("###");
		    return arr;
		   		    
		}
		catch (Exception e)
		{
			String[] blank = {};
			return blank;
		}
	}
	@Override
	public void onClick(View v) {

		if (v.getId()==R.id.edit_user_button)
		{
			startActivity(new Intent(this,Edit_details1.class));
		}
		else if (v.getId()==R.id.show_me)
		{
			AlertDialog  art = new AlertDialog.Builder(MainActivity.this).create();
			try
			{
				SharedPreferences sharedPreferences = getSharedPreferences("HelpDesk",MODE_PRIVATE);
			    String strSavedMem1 = sharedPreferences.getString("Last_address", "");
			    
				art.setMessage(strSavedMem1);
				art.show();
			}
			catch (Exception e)
			{
				art.setMessage("Can't get your last location.");
				art.show();
			}
		}
		else if (v.getId()==R.id.exit_button)
		{
			finish();
		}
		else if (v.getId()==R.id.deactivate_helpdesk)
		{
			Intent myIntent = new Intent(MainActivity.this, MyWarningService.class);
			stopService(myIntent);
		}
	}

	@Override
	public void onLocationChanged(Location location) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void onProviderDisabled(String provider) {

		AlertDialog art = new AlertDialog.Builder(MainActivity.this).create();
		art.setMessage("GPS is turned off.\nPlease turn on GPS.");
		art.show();
	}

	@Override
	public void onProviderEnabled(String provider) {
		// TODO Auto-generated method stub
	}

	@Override
	public void onStatusChanged(String provider, int status, Bundle extras) {
		// TODO Auto-generated method stub

	}


}
