package com.anitam.corry.helpdesk;

public class Constants {
	public static final String DATABASE_NAME="help_desk_db";
	public static final int DATABASE_VERSION=1;
	public static final String TABLE_NAME="emergency_contacts";
	public static final String KEY_ID="_id";
	public static final String CONTACT_NAME="contact_name";
	public static final String CONTACT_PHONE="contact_phone";

}
